package com.company;

public class Operation extends Pet {
    private String operName;
    private int price;

    public Operation(){

    }

    public Operation(String operName, int price){
        setOperName(operName);
        setPrice(price);
    }

    public Operation(String kind, String nickname, int age, String operName, int price){
        super(kind, nickname, age);
        setOperName(operName);
        setPrice(price);
    }

    public Operation(String name, String surname, String username, Password password, String kind, String nickname, int age, String operName, int price){
        super(name, surname, username, password, kind, nickname, age);
        setOperName(operName);
        setPrice(price);
    }

    public void setOperName(String operName){
        this.operName = operName;
    }

    public String getOperName(){
        return operName;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public int getPrice(){
        return price;
    }

    public String toString(){
        return super.toString() + " " + operName + " " + price;
    }

}
