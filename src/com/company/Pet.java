package com.company;

import java.util.ArrayList;

public class Pet extends User {
    private String kind;
    private String nickname;
    private int age;
    private ArrayList<Operation> operations;

    public Pet(){

    }

    public Pet(String kind, String nickname, int age){
        setKind(kind);
        setNickname(nickname);
        setAge(age);
        operations = new ArrayList<Operation>();
    }

    public Pet(String name, String surname, String kind, String nickname, int age){
        super(name, surname);
        setKind(kind);
        setNickname(nickname);
        setAge(age);
        operations = new ArrayList<Operation>();
    }

    public Pet(String name, String surname, String username, Password password, String kind, String nickname, int age){
        super(name, surname, username, password);
        setKind(kind);
        setNickname(nickname);
        setAge(age);
        operations = new ArrayList<Operation>();
    }

    public void setKind(String kind){
        this.kind = kind;
    }

    public String getKind(){
        return kind;
    }

    public void setNickname(String nickname){
        this.nickname = nickname;
    }

    public String getNickname(){
        return nickname;
    }

    public void setAge(int age){
        this.age = age;
    }

    public int getAge(){
        return age;
    }

    public void addOperation(Operation operation){
        operations.add(operation);
    }

    public ArrayList<Operation> getOperations(){
        return operations;
    }

    public String toString(){
        return super.toString() + " " + kind + " " + nickname + " " + age;
    }

}
