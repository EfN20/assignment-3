package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MyApplication {
    // users - a list of users
    private ArrayList<User> users;
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private Pet currentPet;

    public MyApplication() throws FileNotFoundException{
        users = new ArrayList<User>();
        fillUsers();
    }

    private void fillUsers() throws FileNotFoundException{
        File file = new File("C:\\Users\\Berikkali\\IdeaProjects\\Testv2\\src\\com\\company\\db.txt");
        Scanner fileScanner = new Scanner(file);
        int id;
        String name, surname, username, password;
        while(fileScanner.hasNextInt()){
            id = fileScanner.nextInt();
            name = fileScanner.next();
            surname = fileScanner.next();
            username = fileScanner.next();
            password = fileScanner.next();
            addUser(new User(id, name, surname, username, new Password(password)));
        }

    }

    public void start() throws IOException {
//        File file = new File("C:\\Users\\Berikkali\\IdeaProjects\\Testv2\\src\\com\\company");
//        Scanner fileScanner = new Scanner(file);
        fillUsers();
        // fill userlist from db.txt

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else if(choice == 2) {
                break;
            }
        }
        saveUserList();
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice3 = sc.nextInt();
                if (choice3 == 1) authentication();
                else break;
            }
            else if(signedUser != null) {
                userProfile();
                break;
            }
        }
    }

    private void authentication() throws IOException {
        // sign in
        // sign up
        while (true){
            System.out.println("If you are new here, create new account.");
            System.out.println("Already have a account?");
            System.out.println("1. Sign up");
            System.out.println("2. Sign in");
            System.out.println("3. Exit");
            int choice2 = sc.nextInt();
            if(choice2 == 1){
                signUp();
            }
            else if(choice2 == 2){
                signIn();
            }
            else break;
        }
    }

    private void saveUserList() throws IOException {
        File userWrite = new File("C:\\Users\\Berikkali\\IdeaProjects\\Testv2\\src\\com\\company\\db.txt");
        FileWriter userWrite2 = new FileWriter(userWrite, false);
        for(int i = 0; i < users.size(); i++){
            userWrite2.write((users.get(i)).toString());
            userWrite2.write(System.getProperty( "line.separator" ));
        }
        userWrite2.close();

        File petWrite = new File("C:\\Users\\Berikkali\\IdeaProjects\\Testv2\\src\\com\\company\\petdb.txt");
        FileWriter petWrite2 = new FileWriter(petWrite, false);
        File operWrite = new File("C:\\Users\\Berikkali\\IdeaProjects\\Testv2\\src\\com\\company\\operdb.txt");
        FileWriter operWrite2 = new FileWriter(operWrite, false);
        for(int i = 0; i < users.size(); i++){
            ArrayList<Pet> pets = (users.get(i)).getPet();
            for(int j = 0; j < pets.size(); j++){
                ArrayList<Operation> opers = (pets.get(j)).getOperations();
                for(int k = 0; k < opers.size(); k++){
                    operWrite2.write((opers.get(k)).toString());
                    operWrite2.write(System.getProperty( "line.separator" ));
                }
                petWrite2.write((pets.get(j)).toString());
                petWrite2.write(System.getProperty( "line.separator" ));
            }
        }
        petWrite2.close();
        operWrite2.close();

//        FileOutputStream fileWrite2 = new FileOutputStream(fileWrite);
//        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fileWrite2));
//        for (int i = 0; i < users.size(); i++){
//            bw.write((users.get(i)).toString());
//            bw.newLine();
//        }
//        bw.close();
    }

    private void addUser(User user) {
        users.add(user);
    }

    private void userProfile() throws IOException {
        while (true){
            System.out.println("You are signed in.");
            System.out.println("1. Edit profile");
            System.out.println("2. Add pet");
            System.out.println("3. Your pets");
            System.out.println("4. Log off");
            int choice = sc.nextInt();
            if(choice == 1){
                System.out.println("What you want to edit?");
                System.out.println("1. Name");
                System.out.println("2. Surname");
                System.out.println("3. Username");
                System.out.println("4. Password");
                System.out.println("5. Back");
                int choice4 = sc.nextInt();
                switch (choice4){
                    case 1:
                        System.out.println("Enter your new name: ");
                        String name = sc.next();
                        signedUser.setName(name);
                        break;

                    case 2:
                        System.out.println("Enter your new surname: ");
                        String surname = sc.next();
                        signedUser.setSurname(surname);
                        break;

                    case 3:
                        System.out.println("Enter your new username: ");
                        String username = sc.next();
                        signedUser.setUsername(username);
                        break;

                    case 4:
                        System.out.println("Enter your new password: ");
                        String password = sc.next();
                        (signedUser.getPassword()).setPasswordStr(password);
                        break;

                    default:
                        break;
                }
            }
            else if(choice == 2){
                System.out.println("Enter pets kind: ");
                String kind = sc.next();
                System.out.println("Enter pets nickname: ");
                String nickname = sc.next();
                System.out.println("Enter pets age");
                int age = sc.nextInt();
                signedUser.addPet(new Pet(kind, nickname, age));
            }
            else if(choice == 3){
                ArrayList<Pet> pets = signedUser.getPet();
                for(int i = 1; i <= pets.size() + 1; i++){
                    if(i <= pets.size()){
                        System.out.println(i + " " + (pets.get(i-1)).getKind() + " " + (pets.get(i-1)).getNickname() +
                                " " + (pets.get(i-1)).getAge());
                    }
                    else{
                        System.out.println(i + " Back");
                    }
                }
                System.out.println("Select to which one do you want operation: ");
                int choice5 = sc.nextInt();
                if(choice5 > pets.size()){
                    break;
                }
                else{
                    currentPet = pets.get(choice5 - 1);
                    petProfile();
                    break;
                }

            }
            else{
                logOff();
                break;
            }
        }
    }

    private void petProfile() throws IOException {
        while(true){
            System.out.println("Welcome to " + currentPet.getNickname() + " profile");
            System.out.println("1. Do operations");
            System.out.println("2. Operations history");
            System.out.println("3. Back");
            int choice6 = sc.nextInt();
            if(choice6 == 1){
                System.out.println("Which operation do you want to do?");
                String operation = sc.next();
                System.out.println("For what price?");
                int price = sc.nextInt();
                currentPet.addOperation(new Operation(operation, price));
            }
            else if(choice6 == 2){
                for(int i = 0; i < (currentPet.getOperations()).size(); i++){
                    Operation currOper = (currentPet.getOperations()).get(i);
                    System.out.println((i+1) + " " + currOper.getOperName() + " " + currOper.getPrice());
                }
            }
            else{
                userProfile();
                break;
            }
        }
    }

    private void signIn() throws IOException {
        File file = new File("C:\\Users\\Berikkali\\IdeaProjects\\Testv2\\src\\com\\company\\db.txt");
        Scanner fileFinder = new Scanner(file);
        if(signedUser == null){
            int id;
            String name, surname, username, password;
            System.out.println("Your username: ");
            username = sc.next();
            System.out.println("Your password: ");
            password = sc.next();
            for(int i = 0; i < users.size(); i++){
                if (username.equals ((users.get(i)).getUsername()) ){
                    if(password.equals (((users.get(i)).getPassword()).getPasswordStr()) ){
                        System.out.println("Welcome back " + users.get(i).getName() + " " + users.get(i).getSurname() + "!");
                        signedUser = users.get(i);
                        userProfile();
                        break;
                    }
                }
            }
            System.out.println("Incorrect username or password. Please make sure they are correct.");
            signIn();
        }
    }

    private void signUp() throws IOException {
        String name, surname, username, password;
        System.out.println("Please enter your name: ");
        name = sc.next();
        System.out.println("Please enter your surname: ");
        surname = sc.next();
        System.out.println("Please enter your username: ");
        username = sc.next();
        System.out.println("Please enter your password: ");
        password = sc.next();
        addUser(new User(name, surname, username, new Password(password)));
        signIn();
    }

    private void logOff() throws IOException {
        signedUser = null;
        start();
    }

}
