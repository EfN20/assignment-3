package com.company;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String passwordStr;

    public Password(String passwordStr){
        setPasswordStr(passwordStr);
    }

    public void setPasswordStr(String passwordStr){
        if (checkPasswordFormat(passwordStr)) {
            this.passwordStr = passwordStr;
        }
        else{
            System.out.println("Password length must be more than 9 symbols");
        }
    }

    public String getPasswordStr(){
        return passwordStr;
    }

    private static boolean checkPasswordFormat(String passwordStr){
        int passLength = passwordStr.length();
        if(passLength < 10){
            return false;
        }
        else return true;
    }

}
