package com.company;

import java.util.ArrayList;

public class User {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password
    private static int gen_id = 1;
    private int id;
    private String name;
    private String surname;
    private String username;
    private Password password;
    private ArrayList<Pet> pets;

    public User(){

    }

    public User(String name, String surname){
        generateId();
        setName(name);
        setSurname(surname);
        pets = new ArrayList<Pet>();
    }

    public User(String name, String surname, String username){
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        pets = new ArrayList<Pet>();
    }

    public User(String name, String surname, String username, Password password){
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        pets = new ArrayList<Pet>();
    }

    public User(int id, String name, String surname, String username, Password password){
        this.id = id;
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        pets = new ArrayList<Pet>();
    }

    public void generateId(){
        id = gen_id++;
    }
    public int getId(){
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public String getSurname(){
        return surname;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return username;
    }

    public void setPassword(Password password){
        this.password = password;
    }

    public Password getPassword(){
        return password;
    }

    public void addPet(Pet pet){
        pets.add(pet);
    }

    public ArrayList<Pet> getPet(){
        return pets;
    }

    @Override
    public String toString(){
        return id + " " + name + " " + surname + " " + username + " " + password.getPasswordStr();
    }

}
